import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MisionComponent } from '../mision/mision.component';
import { VisionComponent } from '../vision/vision.component';
const routes: Routes = [
  { path: '', component: MisionComponent },
  { path: 'vision', component: VisionComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
